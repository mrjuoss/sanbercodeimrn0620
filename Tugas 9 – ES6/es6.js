// Soal No 1
// const golden = function goldenFunction() {
//   console.log("this is golden!!");
// }

// golden();

const golden = () => {
  console.log("this is golden!!");
}

golden();

// Soal No 2
// const newFunction = function literal(firstName, lastName) {
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function () {
//       console.log(firstName + " " + lastName)
//       return
//     }
//   }
// }

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    }
  }
}

//Driver Code
newFunction("William", "Imoh").fullName();

// Soal No 3
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const { firstName, lastName, destination, occupation } = newObject;

console.log(firstName, lastName, destination, occupation);

// Soal No 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);
//Driver Code
const combined = [...west, ...east];
console.log(combined);

// Soal No 5
const planet = "earth";
const view = "glass";
// var before = 'Lorem ' + view + 'dolor sit amet, ' +
//   'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//   'incididunt ut labore et dolore magna aliqua. Ut enim' +
//   ' ad minim veniam';

let before = `
Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
`;

// Driver Code
console.log(before)