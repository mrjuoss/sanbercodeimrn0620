/**
 * Tugas 3
 * Looping
*/

// Soal No 1 - Looping menggunakan While
console.log('LOOPING PERTAMA');
var angka = 2;
while (angka <= 20 ) {
  console.log(`${angka} - I love coding`);
  angka += 2;
}

console.log("\n");

console.log('LOOPING KEDUA');
var angka2 = 20;
while (angka2 > 0) {
  console.log(`${angka2} - I will become a mobile developer`);
  angka2 -= 2;
}
console.log("\n");

// Soal No 2 - Looping menggunakan For
for(i = 1; i <= 20; i++ ) {
  if (i % 2 === 1) {
    if (i % 3 === 0) {
      console.log(`${i} - I Love Coding`);
    } else {
      console.log(`${i} - Santai`);
    }
  } else if ( i % 2 === 0) {
    console.log(`${i} - Berkualitas`);
  }
}

console.log("\n");
// Soal No 3 - Persegi panjang
// const header = "This is my header";
// console.log(header);
// console.log('-'.repeat(header.length));

var msg = '';
var panjang = 8;
var lebar = 4;
for (var a1 = 1; a1 <= lebar; a1++) {
  for (var b1 = 1; b1 <= panjang; b1++) {
    msg += '#';
  }
  msg += '\n';
}
console.log(msg);

// Soal No 4 - Tangga
var tangga = '';
for (var b = 1; b <= 7; b++) {
  for (var c = 1; c <= b; c++) {
    tangga += '#';
  }
  for (var d = 1; d <= b; d++) {
    tangga += ' ';
  }
  tangga += '\n';
}
console.log(tangga);

// Soal No 5 - Papan Catur
var pesan = '';
for (var a1 = 1; a1 <= 8; a1++) {
  if (a1 % 2 === 1) {
    for (var b1 = 1; b1 <= 1; b1++) {
      pesan += " # # #";
    }
  } else {
    for (var b1 = 1; b1 <= 1; b1++) {
      pesan += "# # # ";
    }
  }

  pesan += '\n';
}
console.log(pesan);
