# IndonesiaMengoding
# InfoQuiz

## Silahkan buat folder baru seperti pada pengerjaan tugas dengan nama Quiz 1
## Masukkan file 001.js, 002.js dan 003.js ke dalam folder tersebut
## Setelah selesai mengerjakan, push pekerjaan Anda ke repository gitlab masing-masing
## Kirim email (akun sanbercode) dan link commit hasil pekerjaan Anda ke akun telegram @Mukhlis_Hanafi

## Kerjakan soal semampunya dengan maksimal dan jujur
## Tidak perlu bertanya kepada trainer terkait soal, cukup dikerjakan sesuai pemahaman peserta
## Peserta diperbolehkan untuk googling, namun tidak diperbolehkan untuk bertanya, berdiskusi, atau mencontek.
## Selama quiz berlangsung, Anda tidak diperkenankan untuk menggunakan grup diskusi atau fasilitas lain untuk menghubungi orang lain dalam rangka bertanya maupun berdiskusi

# Selamat mengerjakan