/*

  A. Papan Ular Tangga (35)
    Buatlah sebuah function ularTangga yang ketika function tersebut dipanggil akan menampilkan papan ular tangga ukuran 10 x 10.

    Output:
    100 99 98 97 96 95 94 93 92 91
    81 82 83 84 85 86 87 88 89 90
    80 79 78 77 76 75 74 73 72 71
    61 62 63 64 65 66 67 68 69 70
    60 59 58 57 56 55 54 53 52 51
    41 42 43 44 45 46 47 48 49 50
    40 39 38 37 36 35 34 33 32 31
    21 22 23 24 25 26 27 28 29 30
    20 19 18 17 16 15 14 13 12 11
    1 2 3 4 5 6 7 8 9 10

*/

function ularTangga() {
  // Tulis code kamu di sini
  var input = [
    [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
    [30, 29, 28, 27, 26, 25, 24, 23, 22, 21],
    [31, 32, 33, 34, 35, 36, 37, 38, 39, 40],
    [50, 49, 48, 47, 46, 45, 44, 43, 42, 41],
    [51, 52, 53, 54, 55, 56, 57, 58, 59, 60],
    [70, 69, 68, 67, 66, 65, 64, 63, 62, 61],
    [71, 72, 73, 74, 75, 76, 77, 78, 79, 80],
    [90, 89, 88, 87, 86, 85, 84, 83, 82, 90],
    [91, 92, 93, 94, 95, 96, 97, 98, 99, 100]
  ];

  var baris1 = input[9];
  var baris2 = input[8];
  var baris3 = input[7];
  var baris4 = input[6];
  var baris5 = input[5];
  var baris6 = input[4];
  var baris7 = input[3];
  var baris8 = input[2];
  var baris9 = input[1];
  var baris10 = input[0];

  var hasil = `
${baris1.reverse()}
${baris2.reverse()}
${baris3.reverse()}
${baris4.reverse()}
${baris5.reverse()}
${baris6.reverse()}
${baris7.reverse()}
${baris8.reverse()}
${baris9.reverse()}
${baris10.reverse()}`;

  var temp = hasil.toString();
  temp = temp.split(',').join(" ");
  return temp;

}

// TEST CASE Ular Tangga
console.log(ularTangga())
/*
Output :
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/