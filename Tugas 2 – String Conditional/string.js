/**
 * Soal No. 1
 * Membuat kalimat
*/

var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

// Output yang diharapkan
// JavaScript is awesome and I love it!
var result = `${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`;

console.log(result);

/**
 * Soal No. 2
 * Mengurai kalimat (Akses karakter dalam string)
*/

var sentence = "I am going to be React Native Developer";

/**
 * Output yang diharapkan
  First word: I
  Second word: am
  Third word: going
  Fourth word: to
  Fifth word: be
  Sixth word: React
  Seventh word: Developer
*/

console.log('First Word: ' + sentence.charAt(0));
console.log('Second Word: ' + sentence.substr(2,3));
console.log('Third Word: ' + sentence.substr(5,5));
console.log('Fourth Word: ' + sentence.substr(11,2));
console.log('Fifth Word: ' + sentence.substr(14,2));
console.log('Sixth Word: ' + sentence.substr(17,5));
console.log('Seventh Word: ' + sentence.substr(30,10));

/**
 * Soal No. 3
 * Mengurai Kalimat (Substring)
*/

var sentence2 = 'wow JavaScript is so cool';
// Output yang diharapkan
// First Word: wow
// Second Word: JavaScript
// Third Word: is
// Fourth Word: so
// Fifth Word: cool

var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14); // do your own!
var thirdWord2 = sentence2.substring(15, 17); // do your own!
var fourthWord2 = sentence2.substring(18, 20); // do your own!
var fifthWord2 = sentence2.substring(21, 25); // do your own!

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

/**
 * Soal No. 4
 * Mengurai Kalimat dan Menentukan Panjang String
*/

var sentence3 = 'wow JavaScript is so cool';

// Output yag diharapkan
// First Word: wow, with length: 3
// Second Word: JavaScript, with length: 10
// Third Word: is, with length: 2
// Fourth Word: so, with length: 2
// Fifth Word: cool, with length: 4

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14); // do your own!
var thirdWord3 = sentence3.substring(15, 17); // do your own!
var fourthWord3 = sentence3.substring(18, 20); // do your own!
var fifthWord3 = sentence3.substring(21, 25); // do your own!

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);