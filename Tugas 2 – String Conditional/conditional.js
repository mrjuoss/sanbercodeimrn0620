// Soal If Else
var nama = prompt("Silahkan ketikkan nama anda !");

if (nama.length === 0) {
  alert("Nama harus diisi!!");
} else {
  console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
}

var peran = prompt("Silahkan memilih peran, penyihir, guard, atau werewolf");

if (peran.length === 0) {
  alert("Pilih Peranmu untuk memulai game");
} else {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
  console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
}

// Soal Switch Case

var tanggal = 17;
var bulan = 2;
var tahun = 1987;

switch (bulan) {
  case 1:
    console.log(`${tanggal} Januari ${tahun}`);
    break;
  case 2:
    console.log(`${tanggal} Februari ${tahun}`);
    break;
  case 3:
    console.log(`${tanggal} Maret ${tahun}`);
    break;
  case 4:
    console.log(`${tanggal} April ${tahun}`);
    break;
  case 5:
    console.log(`${tanggal} Mei ${tahun}`);
    break;
  case 6:
    console.log(`${tanggal} Juni ${tahun}`);
    break;
  case 7:
    console.log(`${tanggal} Juli ${tahun}`);
    break;
  case 8:
    console.log(`${tanggal} Agustus ${tahun}`);
    break;
  case 9:
    console.log(`${tanggal} September ${tahun}`);
    break;
  case 10:
    console.log(`${tanggal} Oktober ${tahun}`);
    break;
  case 11:
    console.log(`${tanggal} November ${tahun}`);
    break;
  case 12:
    console.log(`${tanggal} Desember ${tahun}`);
    break;
  default:
    console.log("Bulan tidak valid");
    break;
}
