// Soal No 1 (Array to Object)

function arrayToObject(arr)
{
  var obj = {};

  var lengthArr = arr.length;

  if (lengthArr === 0) {
    console.log("");
  } else {
    for (var i= 0; i < lengthArr; i++) {
      obj.firstName = arr[i][0];
      obj.lastName = arr[i][1];
      obj.gender = arr[i][2];

      var now = new Date();
      var thisYear = now.getFullYear();

      if (arr[i][3] === undefined) {
        obj.age = '"Invalid Birth Year"';
      } else {
        if (arr[i][3] > thisYear) {
          obj.age = '"Invalid Birth Year"';
        } else {
          obj.age = thisYear - arr[i][3];
        }

      }

      console.log(`
      ${i+1}. ${obj.firstName} ${obj.lastName} : {
            firstName : "${obj.firstName}",
            lastName : "${obj.lastName}",
            gender : "${obj.gender}",
            age : ${obj.age}
          }
  `);
    }
  }

}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
arrayToObject(people2);

arrayToObject([]);

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
  // you can only write your code here!
  var itemSale = [
    ["Sepatu brand Stacattu", 1500000],
    ["Baju brand Zoro", 500000],
    ["Baju brand H&N", 250000],
    ["Sweater brand Uniklooh", 175000],
    ["Casing Handphone", 50000]
  ];

  if (memberId === undefined || memberId === "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  }

  var obj = {
    memberId: memberId,
    money: money,
    listPurchased: [],
    changeMoney: null
  }

  for (var i = 0; i < itemSale.length; i++) {
    if (money >= itemSale[i][1]) {
      obj.listPurchased.push(itemSale[i][0]);
      money -= itemSale[i][1];
    }
  }
  obj.changeMoney = money;
  return obj;

}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No 3 (Naik Angkot)

function naikAngkot(arrPenumpang)
{
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];

  if (arrPenumpang.length === 0) {
    return [];
  }

  var output = [];
  var obj = {};

  for (var i = 0; i < arrPenumpang.length; i++) {
    obj.penumpang = arrPenumpang[i][0];
    obj.naikDari = arrPenumpang[i][1];
    obj.tujuan = arrPenumpang[i][2];

    var jumlahRute = rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]);

    obj.bayar = 2000 * jumlahRute;

    output.push(obj);

    obj = {};

  }
  return output;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]