// Soal No 1

function range(startNum, finishNum)
{
  if (startNum === undefined || finishNum === undefined) {
    return -1;
  } else {
    var arrayHasil = [];
    if (startNum > finishNum) {
      for (var i = startNum; i >= finishNum; i--) {
        arrayHasil.push(i);
      }
    } else {
      for (var i = startNum; i <= finishNum; i++) {
        arrayHasil.push(i);
      }
    }

    return arrayHasil;
  }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

// Soal No 2

function rangeWithStep(startNum, finishNum, step)
{
  if (startNum === undefined || finishNum === undefined) {
    return -1;
  } else {
    var arrayHasil = [];
    if (startNum > finishNum) {
      for (var i = startNum; i >= finishNum; i -= step) {
        arrayHasil.push(i);
      }
    } else {
      for (var i = startNum; i <= finishNum; i += step) {
        arrayHasil.push(i);
      }
    }
    return arrayHasil;
  }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

// Soal No 3

function sum(startNum, finishNum, step)
{
  var sumArray;

  if (step === undefined) step = 1;

  if (startNum === undefined & finishNum === undefined) {
    return 0;
  } else if (finishNum === undefined) {
    return 1;
  }
  else {
    var tempArray = [];
    if (startNum > finishNum) {
      for (var i = startNum; i >= finishNum; i -= step) {
        tempArray.push(i);
      }
    } else {
      for (var i = startNum; i <= finishNum; i += step) {
        tempArray.push(i);
      }
    }
    sumArray = tempArray.reduce(function(a, b) {
      return a + b;
    }, 0);
    return sumArray;
  }
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

// Soal No 4

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(param)
{
  var result = "";
  for (var i = 0; i < param.length; i++) {
    result += `
      Nomor ID : ${param[i][0]}
      Nama Lengkap : ${param[i][1]}
      TTL : ${param[i][2]} ${param[i][3]}
      Hobi : ${param[i][4]}
    `;
  }
  result += '\n';
  return result;
}

console.log(dataHandling(input));

// Soal No 5

function balikKata (kata)
{
  var hasil = "";
  for (var i = kata.length - 1; i >= 0; i --) {
    hasil += kata[i];
  }
  return hasil;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

// Soal No. 6
var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2 (input)
{
  // splice = mengubah array (hapus dan atau tambah element)
  input.splice(1, 1, "Roman Alamsyah Elsharawy");
  input.splice(2, 1, "Provinsi Bandar Lampung");
  input.splice(4, 0, "Pria");
  input.splice(5, 1, "SMA Internasional Metro");

  console.log(input);

  var tanggal = input[3];
  var splitTanggal = tanggal.split("/");

  var bulan = "";

  // console.log(splitTanggal[1]);

  switch (splitTanggal[1]) {
    case '01':
      bulan = "Januari";
      break;
    case '02':
      bulan = "Februari";
      break;
    case '03':
      bulan = "Maret";
      break;
    case '04':
      bulan = "April";
      break;
    case '05':
      bulan = "Mei";
      break;
    case '06':
      bulan = "Juni";
      break;
    case '07':
      bulan = "Juli";
      break;
    case '08':
      bulan = "Agustus";
      break;
    case '09':
      bulan = "September";
      break;
    case '10':
      bulan = "Oktober";
      break;
    case '11':
      bulan = "November";
      break;
    case '12':
      bulan = "Desember";
      break;
    default:
      bulan = "Bulan tidak valid";
      break;
  }
  console.log(bulan);

  // Sorting
  var resultSortingDesc = splitTanggal.sort().reverse();
  console.log(resultSortingDesc)

  // Join
  var resultJoin = input2[3].split("/").join("-");
  console.log(resultJoin);
  // Slice
  var nama = input2[1].slice(0, 15);
  console.log(nama);
}

dataHandling2(input2);
