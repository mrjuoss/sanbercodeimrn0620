/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 *
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 *
 * Selamat mengerjakan
*/

/*==========================================
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email".
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(subject,  points, email) {
    this.subject = subject;
    this.points = points;
    this.email = email;
  }

  average = () => {
    let temp = 0;
    if (Array.isArray(this.points)) {
      for (let i = 0; i < this.points.length; i ++) {
        temp += this.points[i];
      }
      return (temp / this.points.length).toFixed(2);
    } else {
      return this.points;
    }
  }
}

let kelasA = new Score('Javascript', [9, 2, 4], 'mrjuoss@yahoo.com');
let kelasB = new Score('Javascript', [50, 2, 4], 'mrjuoss@gmail.com');
let kelasC = new Score('Javascript', 90, 'cak-jaki@live.com');

console.log(`Nilai Rata-Rata Kelas A : ${kelasA.average()}`);
console.log(`Nilai Rata-Rata Kelas B : ${kelasB.average()}`);
console.log(`Nilai Rata-Rata Kelas C : ${kelasC.average()}`);
console.log(kelasA.subject);
console.log(kelasA.email);
console.log(kelasB.subject);
console.log(kelasB.email);
/*===========================================
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh:

  Input

  data :
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
  let result = [];
  for (let i= 0; i < data.length; i++) {
    if (subject === 'quiz-1') {
      let DataOne = new Score(subject, data[i][1], data[i][0]);
      let obj = {};
      obj.email = DataOne.email;
      obj.subject = DataOne.subject;
      obj.points = DataOne.average();
      result.push(obj);
    } else if (subject === 'quiz-2') {
      let DataTwo = new Score(subject, data[i][2], data[i][0]);
      let obj = {};
      obj.email = DataTwo.email;
      obj.subject = DataTwo.subject;
      obj.points = DataTwo.average();
      result.push(obj);
    } else if (subject === 'quiz-3') {
      let DataThree = new Score(subject, data[i][3], data[i][0]);
      let obj = {};
      obj.email = DataThree.email;
      obj.subject = DataThree.subject;
      obj.points = DataThree.average();
      result.push(obj);
    }
  }
  console.log(result);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student.
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan.
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  // code kamu di sini
  let result = '';
  for (let i = 1; i < data.length; i++) {
    let DataFour = new Score('React Native', data[i].slice(1), data[i][0]);
    let email = DataFour.email;
    let rataRata = DataFour.average();

    result += `${i}. Email: ${email} \n`
    result += `Rata-rata: ${rataRata} \n`

    if (rataRata > 90) {
      result += `Predikat: honour \n`;
    } else if (rataRata > 80) {
      result += `Predikat: graduate \n`;
    } else if (rataRata > 70) {
      result += `Predikat: participant \n`;
    }
    result += `\n`;
  }
  console.log(result);
}

recapScores(data);
