// Soal No 1 - Animal Class
// Release 0

class Animal
{
  constructor(_name)
  {
    this._name = _name;
    this.legs = 4;
    this.cold_blooded = false;
  }

  set name(_name)
  {
    this._name = _name;
  }

  get name()
  {
    return this._name;
  }

}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

// Release 1

class Frog extends Animal
{
  jump()
  {
    console.log("hop hop");
  }
}

class Ape extends Animal
{
  constructor(_name)
  {
    super(_name);
    this.legs = 2;
  }

  yell()
  {
    console.log("Auooo");
  }
}

var sungokong = new Ape("kera sakti")
sungokong.yell();
// console.log(sungokong.legs);

var kodok = new Frog("buduk");
kodok.jump();
// console.log(kodok.legs);

// Soal No 2 - Function to Class

class Clock
{
  constructor({template})
  {
    this.template = template;
  }

  render = () => {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);


    console.log(output);
  }

  stop = () => {
    clearInterval(this.timer);
  };

  start = () => {
    this.render();
    this.timer = setInterval(this.render, 1000);
  };
}

/**
function Clock({ template }) {

  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function () {
    clearInterval(timer);
  };

  this.start = function () {
    render();
    timer = setInterval(render, 1000);
  };

}
 */

var clock = new Clock({ template: 'h:m:s' });
clock.start();