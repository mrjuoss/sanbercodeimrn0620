import React from 'react'
import { View, Text, Image, FlatList, StyleSheet, TouchableOpacity } from 'react-native'

import Card from './components/Card'
import data from './skillData.json'

export default class SkillScreen extends React.Component {
  render() {
  return (
    <View style={styles.container}>
      <Image
        source={require('../Tugas13/assets/logo.png')}
        style={{ marginBottom: 16, width: 150, height: 20, alignSelf: 'flex-start'}}
      />
      <View style={styles.profileBox}>
        <Image
          source={require('../Tugas13/assets/jaki.jpg')}
          style={{ width: 60, height: 60, borderRadius: 50 }}
        />
        <View style={styles.profileDetailBox}>
          <Text style={{ color: '#003366', fontWeight: 'bold' }}>
            Mohamad Arif Mujaki
          </Text>
          <Text style={{ color: '#1E4469' }}>
            Fullstack Developer, UI/UX Engineer
          </Text>
          <Text style={{ color: '#535B63' }}>
            DKI Jakarta, Indonesia
          </Text>
        </View>

      </View>

      <View style={styles.skillBox}>
        <Text style={{
          alignSelf: 'flex-start',
          color: "gray",
          marginTop: 24,
          marginBottom: 10,
          fontSize: 30,
          fontWeight: "bold"
        }}>
          MY SKILL
            </Text>
        <View style={styles.skillCategory}>
          <View style={styles.skillList}>
            <Text style={{ fontSize: 11, color: "white" }}>FRAMEWORK</Text>
          </View>
          <View style={styles.skillList}>
            <Text style={{ fontSize: 11, color: "white" }}>BAHASA PEMROGRAMAN</Text>
          </View>
          <View style={styles.skillList}>
            <Text style={{ fontSize: 11, color: "white" }}>TEKNOLOGI</Text>
          </View>
        </View>
        <FlatList
          data={data.items}
          renderItem={(skill) => <Card skill={skill} />}
          keyExtractor={(item) => item.id}
        />
      </View>

      <Text style={{ marginTop: 32, fontWeight: 'bold', color: '#003366' }}>Social Media</Text>
      <View style={styles.socialMediaBox}>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('../Tugas13/assets/icon_fb.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              Sanber Code Indonesia
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('../Tugas13/assets/icon_twitter.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCodeIndonesia
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('../Tugas13/assets/icon_ig.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCodeOfficial
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('../Tugas13/assets/icon_gitlab.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCode
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('../Tugas13/assets/icon_github.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCode
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 32
  },
  profileBox: {
    flexDirection: 'row'
  },
  profileDetailBox: {
    flexDirection: 'column',
    marginLeft: 8
  },
  skillBox: {

  },
  skillCategory: {
    flexDirection: "row",
    marginBottom: 10
  },
  skillList: {
    backgroundColor: "#003366",
    marginHorizontal: 5,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 8
  },
  socialMediaBox: {
    marginTop: 8
  },
  socialMediaDetail: {
    flexDirection: 'row',
    marginBottom: 8
  },
})