import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default function Card(props) {

  return (
    < View style={styles.container}>
      <Icon name={props.skill.item.iconName} size={80} color="white"></Icon>
      <View style={styles.skillDetail}>
        <Text style={{ color: "white", fontWeight: 'bold', fontSize: 24 }}>{props.skill.item.skillName}</Text>
        <Text style={{ color: "white", fontSize: 16 }}>{props.skill.item.categoryName}</Text>
        <Text style={{ fontSize: 48, color: 'white', alignSelf: 'flex-end', fontWeight: 'bold', marginTop: -10 }}>{props.skill.item.percentageProgress}</Text>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: "#003366",
    elevation: 5,
    borderRadius: 10,
    justifyContent: "space-around",
    alignItems: 'center',
    marginBottom: 10
  },
  skillDetail: {
    paddingVertical: 16,
    display: 'flex',
    alignItems: 'flex-start'
  }
})
