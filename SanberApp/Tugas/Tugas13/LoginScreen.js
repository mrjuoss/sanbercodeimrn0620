import React from 'react'
import { View, Image, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'

const LoginScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.boxLogo}>
        <Image
          source= {require('./assets/logo.png')}
          style={{width: 100, height: 15}}
        />
        <Text style={styles.textLogo}>
          SANBER SKILL
        </Text>
        <Text style={styles.subTextLogo}>
          Show Your Skill to Word
        </Text>
      </View>
      <View style={styles.boxForm}>
        <Text style={styles.textForm}>
          Username/ Email
        </Text>
        <TextInput style={styles.inputForm}
        />
        <Text style={styles.textForm}>
          Password
        </Text>
        <TextInput style={styles.inputForm} />
      </View>
      <View style={styles.boxButton}>
        <TouchableOpacity style={styles.btnSignIn}>
          <Text style={{ color: 'white'}}>SIGN IN</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnSignUp}>
          <Text style={{ color: '#003366'}}>SIGNUP</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 64
  },
  boxLogo: {
    paddingTop: 16
  },
  textLogo: {
    fontSize: 36,
    fontWeight: 'bold',
    color:'#003366'
  },
  subTextLogo: {
    fontSize: 12,
    color: '#003366'
  },
  textForm: {
    fontSize: 14
  },
  inputForm: {
    marginTop: 8,
    marginBottom: 16,
    height: 40,
    paddingLeft: 16,
    borderColor: '#B0B3B6',
    borderWidth: 1,
    borderRadius: 4,
  },
  boxForm: {
    marginTop: 64
  },
  boxButton: {
    marginTop: 32,
    flexDirection: 'row'
  },
  btnSignIn: {
    width: 150,
    fontSize: 12,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    backgroundColor: '#003366',
    borderRadius: 8
  },
  btnSignUp: {
    width: 150,
    marginLeft: 16,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    fontSize: 12,
    backgroundColor: 'white',
    color: '#003366',
    borderColor: '#003366',
    borderWidth: 1,
    borderRadius: 8
  }
})

export default LoginScreen
