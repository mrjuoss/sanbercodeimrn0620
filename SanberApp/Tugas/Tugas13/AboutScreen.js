import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'

const AboutScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.profileBox}>
        <Image
          source={require('./assets/jaki.jpg')}
          style={{ width: 60, height: 60, borderRadius: 50}}
        />
        <View style={styles.profileDetailBox}>
          <Text style={{ color: '#003366', fontWeight: 'bold'}}>
            Mohamad Arif Mujaki
          </Text>
          <Text style={{ color: '#1E4469' }}>
            Fullstack Developer, UI/UX Engineer
          </Text>
          <Text style={{ color: '#535B63' }}>
            DKI Jakarta, Indonesia
          </Text>
        </View>

      </View>
      <Text style={{ marginTop: 32, fontWeight: 'bold', color: '#003366' }}>Social Media</Text>
      <View style={styles.socialMediaBox}>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('./assets/icon_fb.png')}
              style={{ width: 20}}
            />
            <Text style={{ marginLeft: 8}}>
              Sanber Code Indonesia
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('./assets/icon_twitter.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCodeIndonesia
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('./assets/icon_ig.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCodeOfficial
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('./assets/icon_gitlab.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCode
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.socialMediaDetail}>
            <Image
              source={require('./assets/icon_github.png')}
              style={{ width: 20 }}
            />
            <Text style={{ marginLeft: 8 }}>
              SanberCode
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <Text style={{ marginTop: 32, color: '#003366', fontWeight: 'bold' }}>PortoFolio</Text>
      <View style={styles.portoFolioBox}>
        <View style={styles.portoFolioDetail}>
          <Image
            source={require('./images/portofolio_1.png')}
            style={{width: 140, height: 100}}
          />
          <Text style={styles.portoFolioText}>
            Learning Management System
          </Text>
        </View>
        <View>
          <Image
            source={require('./images/portofolio_2.png')}
            style={{ width: 140, height: 100 }}
          />
          <Text style={styles.portoFolioText}>
            Restaurant Website
          </Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 32
  },
  profileBox: {
    flexDirection: 'row'
  },
  profileDetailBox: {
    flexDirection: 'column',
    marginLeft: 8
  },
  socialMediaBox: {
    marginTop: 8
  },
  socialMediaDetail: {
    flexDirection: 'row',
    marginBottom: 8
  },
  portoFolioBox: {
    flexDirection: 'row',
    marginTop: 16
  },
  portoFolioDetail: {
    marginRight: 16
  },
  portoFolioText: {
    fontSize: 10,
    paddingTop: 8
  }
})
export default AboutScreen