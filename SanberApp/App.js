import React from 'react'
import { View, StyleSheet } from 'react-native'

// import YoutubeUI from './Tugas/Tugas12/App'

// import LoginScreen from './Tugas/Tugas13/LoginScreen'
// import AboutScreen from './Tugas/Tugas13/AboutScreen'
// import ToDoApp from './Tugas/Tugas14/App'
// import SkillScreen from './Tugas/Tugas14/SkillScreen'
// import ReactNavTutorial from './Tugas/Tugas15/index'

// import GithubUser from './Latihan/Component/GithubUser'
// import Quiz3 from './Quiz3/index'

// import StateProps101 from './Latihan/Basics/StateProps101'

// import Image101 from './Latihan/Basics/Image101'
// import Alert101 from './Latihan/Component/Alert101'
import FetchDataAPI from './Latihan/FetchDataAPI'


export default class App extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        {/* <YoutubeUI /> */}
        {/* <LoginScreen /> */}
        {/* <AboutScreen /> */}
        {/* <ToDoApp /> */}
        {/* <SkillScreen /> */}
        {/* <ReactNavTutorial /> */}

        {/* <GithubUser /> */}
        {/* <Quiz3 /> */}
        {/* <StateProps101 /> */}
        {/* <Image101 /> */}
        {/* <Alert101 /> */}
        <FetchDataAPI />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 32,
    marginBottom: 8,
    padding: 16
  }
})