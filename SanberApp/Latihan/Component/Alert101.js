import React, { Component } from 'react'
import { Text, View, Alert, Button, StyleSheet, TouchableOpacity } from 'react-native'

export default class Alert101 extends Component {
  _onPressButton() {
    alert('You tapped the button!')
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ marginVertical: 16 }}>
          <TouchableOpacity
            onPress= {this._onPressButton}
            style = {styles.buttonStyle}
          >
            <Text style={styles.textButton}> Basic Alert </Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginVertical: 16 }}>
          <Button
            title='Positive Netral'
            onPress={() =>
              Alert.alert(
                'Info',
                'Apakah Anda Setuju',
                [
                  {
                    text: 'Tidak',
                    onPress: () => console.log('Tidak Setuju Pressed')
                  },
                  {
                    text: 'Setuju',
                    onPress: () => console.warn('Ok, Setuju')
                  }
                ],
                {
                  cancelable: false
                }
                )
            }
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonStyle: {
    paddingHorizontal: 24,
    backgroundColor: '#003366',
    paddingVertical: 8,
    borderRadius: 8,
  },
  textButton: {
    color: '#fff'
  }
})