import React, { Component } from 'react'
import { FlatList, View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView } from 'react-native'
// import { FlatList, ScrollView, Image, View, Text, StyleSheet } from 'react-native'


const rows = [
  { id: 0, text: 'View' },
  { id: 1, text: 'Text' },
  { id: 2, text: 'Image' },
  { id: 3, text: 'ScrollView' },
  { id: 4, text: 'ListView' },
  { id: 5, text: 'FlatList' },
  { id: 6, text: 'View 2' },
  { id: 7, text: 'Text' },
  { id: 8, text: 'Image' },
  { id: 9, text: 'ScrollView' },
  { id: 10, text: 'ListView' },

]

const extractKey = ({ id }) => id

export default class BasicComponent extends Component {

  constructor() {
    super()
    this.state = {
      count: 3,
      value: 'useless Placeholder',
    }
  }

  onPress() {
    this.setState({ count: this.state.count + 1 })
  }

  renderItem = ({ item }) => {
    return (
      <Text style={styles.row}>
        {item.text}
      </Text>
    )
  }

  render() {
    return (
      <ScrollView>
        <FlatList
          style={styles.container}
          data={rows}
          renderItem={this.renderItem}
          keyExtractor={extractKey}
        />
        <View style={styles.container}>
          <TextInput
            style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={(text) => this.setState({ value: text })}
          />
          <Text>
            {this.state.value}
          </Text>
          <Text>
            Count : {this.state.count}
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.onPress()}
          >
            <Text>Press Here</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
  // render() {
  //   return (
  //     <ScrollView style={styles.container}>
  //       <View style={styles.boxLarge} />
  //       <ScrollView horizontal>
  //         <View style={styles.boxSmall} />
  //         <View style={styles.boxSmall} />
  //         <View style={styles.boxSmall} />
  //       </ScrollView>
  //       <View style={styles.boxLarge} />
  //       <View style={styles.boxSmall} />
  //       <View style={styles.boxLarge} />
  //       <Image
  //         style={styles.image}
  //         source={{ uri: 'http://www.reactnativeexpress.com/static/logo.png' }}
  //       />
  //       <View style={styles.box} />
  //       <Text style={styles.text}>Hello</Text>
  //     </ScrollView>
  //   )
  // }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'skyblue',
    borderWidth: 2,
    borderColor: 'steelblue',
    borderRadius: 20,
  },
  text: {
    backgroundColor: 'whitesmoke',
    color: '#4A90E2',
    fontSize: 24,
    padding: 10,
  },
  image: {
    width: 100,
    height: 100,
  },
  boxLarge: {
    width: 300,
    height: 300,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: 'steelblue'
  },
  boxSmall: {
    width: 200,
    height: 200,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: 'skyblue'
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#dddddd',
    marginTop: 10,
    padding: 10,
    borderRadius: 8,
  }
})