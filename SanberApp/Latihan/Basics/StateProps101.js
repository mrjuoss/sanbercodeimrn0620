import React, { Component } from 'react'
import { Text, View, TextInput, Button } from 'react-native'

export default class State101 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: ''
    }
  }
  render() {
    return (
      <View>
        <Text> Siapa Nama Kamu </Text>
        <TextInput
          placeholder='Ketikkan Nama'
          style={{ width: 200, borderWidth: 1}}
          onChangeText={(text) => this.setState({ name: text})}
        />
        <Text>Nama Kamu : {this.state.name}</Text>

        <Show name={this.state.name} />
      </View>
    )
  }
}

class Show extends Component {
  render() {
    return(
      <View>
        <Text style={{ fontWeight: 'bold'}}>
          Hello props {this.props.name}
        </Text>
      </View>
    )
  }
}

/**
 * Referensi
 * https://www.konsepkoding.com/2020/02/tutorial-react-native-state-dan-props.html
*/
