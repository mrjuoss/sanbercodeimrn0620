import React, { Component } from 'react'
import { Image, View, Dimensions } from 'react-native'

export default class Image101 extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>

        <Image
          style={{ width: 400, height: 400 }}
          resizeMode={'repeat'} // stretch, center, cover, contain, repeat
          source={{ uri: 'https://images.pexels.com/photos/933054/pexels-photo-933054.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }} />
      </View>
    )
  }
}

/**
 * Referensi
 * https://www.konsepkoding.com/2020/02/tutorial-menampilkan-gambar-react-native.html
*/
