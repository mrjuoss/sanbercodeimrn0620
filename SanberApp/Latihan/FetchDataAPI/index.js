import React, { Component } from 'react'
import {
  Text,
  View,
  FlatList,
  StyleSheet,
 } from 'react-native'

const data = [
  { name: 'Mohamad Arif Mujaki' },
  { name: 'Nurul Fauziah' },
  { name: 'Nura Maryam Assyfa' },
  { name: 'Adiknya Nura'}
]

export default class App extends Component {

  state = {
    data: []
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async () => {
    const response = await fetch("https://randomuser.me/api?results=50")
    const json = await response.json()
    this.setState({
      data: json.results
    })


  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          keyExtractor={(x, i) => i}
          renderItem={({item}) => <Text style={{ marginVertical: 8 }}>{`${item.name.first} ${item.name.last}`}</Text>}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {

  }
})
